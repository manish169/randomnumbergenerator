const feathers = require('@feathersjs/feathers')
const express = require('@feathersjs/express')
const app = express(feathers())
const mongoose = require('mongoose')
const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient
const service = require('feathers-mongoose')
// var Random = require('./models/random')

const connectionURL = 'mongodb://127.0.0.1:27017'
const databaseName = 'random'

app.use(express.json())
app.use(express.urlencoded({ extended : true }))
app.configure(express.rest())
app.use(express.errorHandler())

// random number generator
class randomService {

    constructor() {

    }

    // POST localhost:3000/random
    create(data, params) {
        MongoClient.connect(connectionURL, { useNewUrlParser: true }, (error, client) => {
            if (error) {
                return console.log('Unable to connect to the database!')
            }

            const db = client.db(databaseName)

            db.collection('random').insertOne({
                randomNumber: Math.random()
            })
            
        })
        return Promise.resolve( 'The number is generated and stored in Database')
        
    }
    
    // GET localhost:3000/random
    find(params) {
        return Promise.resolve('Handling POST Requests at POST /random')
    }
}

app.use('/random', new randomService())

app.listen(3000, () => console.log('Hello Wold app is running on http://localhost:3000'))